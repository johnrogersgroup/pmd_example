/**
 * Copyright (c) 2015 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "boards.h"
#include "app_error.h"
#include <string.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "nrf_drv_pdm.h"

#define CONFIG_PDM_BUFFER_SIZE_SAMPLES 400

// <o> PDM Decimation Filter Gain <0x00-0x50>
// <i> For details on the PDM decimation filter, see the 'Decimation filter' section in the nRF52 Product Specification document.
#define CONFIG_PDM_GAIN 0x40

// <o> PDM CLK Pin
#define CONFIG_IO_PDM_CLK 0x06

// <o> PDM DATA Pin
#define CONFIG_IO_PDM_DATA 0x05



/**@brief Audio buffer handler. */
typedef void (*drv_audio_buffer_handler_t)(int16_t *p_buffer, uint16_t samples);



static int16_t                      m_pdm_buff[2][CONFIG_PDM_BUFFER_SIZE_SAMPLES];
static volatile bool first = true;
static volatile int count = 0;

static void drv_audio_pdm_event_handler(nrf_drv_pdm_evt_t const * const evt)
{

    if(evt->error == PDM_NO_ERROR) {

        if(evt-> buffer_released != NULL) {

            NRF_LOG_INFO("Data: %d", *(evt->buffer_released));
            NRF_LOG_INFO("Data: %d", *(evt->buffer_released+100));
            NRF_LOG_INFO("Data: %d", *(evt->buffer_released+200));
            NRF_LOG_INFO("Data: %d", *(evt->buffer_released+300));

            if(evt->buffer_requested) {
                nrf_drv_pdm_buffer_set(evt->buffer_released, CONFIG_PDM_BUFFER_SIZE_SAMPLES);
            }
        }

    }
}



uint32_t drv_audio_init()
{
    nrf_drv_pdm_config_t pdm_cfg = NRF_DRV_PDM_DEFAULT_CONFIG(CONFIG_IO_PDM_CLK,
                                                              CONFIG_IO_PDM_DATA);
    
    pdm_cfg.gain_l      = CONFIG_PDM_GAIN;
    
    return nrf_drv_pdm_init(&pdm_cfg, drv_audio_pdm_event_handler);
}


void nrf_logger_init()
{
    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_DEFAULT_BACKENDS_INIT();
    NRF_LOG_INFO("ADXL345 example.");
}
/**
 * @brief Function for application main entry.
 */
int main(void)
{
    uint32_t err_code;

    // Initialization
    nrf_logger_init();
    err_code = drv_audio_init();
    APP_ERROR_CHECK(err_code);


    nrf_drv_pdm_buffer_set(m_pdm_buff[0], CONFIG_PDM_BUFFER_SIZE_SAMPLES);
    nrf_drv_pdm_buffer_set(m_pdm_buff[1], CONFIG_PDM_BUFFER_SIZE_SAMPLES);
    // Start Mic
    err_code =  nrf_drv_pdm_start();
    APP_ERROR_CHECK(err_code);
    while (true)
    {
        // Do nothing.
        NRF_LOG_FLUSH();
    }
}
/** @} */
